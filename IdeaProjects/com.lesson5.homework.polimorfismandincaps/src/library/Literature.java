package library;

public class Literature {
    public String title;
    public String subject;
    public String publisher;
    public int yearOfPublishing;

    public Literature(String title, String subject, String publisher, int yearOfPublishing) {
        this.title = title;
        this.subject = subject;
        this.publisher = publisher;
        this.yearOfPublishing = yearOfPublishing;
    }

    public String getInfo(){
        return title;
    }
}
