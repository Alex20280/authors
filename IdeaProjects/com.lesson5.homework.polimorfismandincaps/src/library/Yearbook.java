package library;

public class Yearbook extends Literature {

    public Yearbook(String title, String subject, String publisher, int yearOfPublishing) {
        super(title, subject, publisher, yearOfPublishing);
    }

    @Override
    public String getInfo() {
        return super.getInfo() + " - yearbook";
    }
}
