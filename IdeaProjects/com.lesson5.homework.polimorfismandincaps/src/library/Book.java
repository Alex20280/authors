package library;

public class Book extends Literature {
    public String author;

    public Book(String title, String subject, String publisher, int yearOfPublishing, String author) {
        super(title, subject, publisher, yearOfPublishing);
        this.author = author;
    }

    @Override
    public String getInfo() {
        return super.getInfo() + " - book";
    }
}
