package library;

public class Magazine extends Literature {
    public String dateOfPublishing;

    public Magazine(String title, String subject, String publisher, int yearOfPublishing, String dateOfPublishing) {
        super(title, subject, publisher, yearOfPublishing);
        this.dateOfPublishing = dateOfPublishing;
    }

    @Override
    public String getInfo() {
        return super.getInfo() + " - magazine";
    }
}
