package library;

public class Main {
    public static void main(String[] args) {

        Literature [] lit = new Literature[3];
        lit [0] = new Book("Book1", "Subject1", "pub1", 2016, "author1");
        lit [1] = new Magazine("Mag1", "Subject2", "pub2", 2016, "25 October");
        lit [2] = new Yearbook("Year1", "Subject3", "pub3", 2017);

        for (Literature literature:lit){
            System.out.println(literature.getInfo());
        }
    }
}
